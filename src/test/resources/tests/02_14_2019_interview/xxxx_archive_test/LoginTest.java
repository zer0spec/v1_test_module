package tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObject.LandingPage;
import pageObject.LaunchApplication;
import pageObject.LoginPage;

public class LoginTest {

    @Before
    public void setUp() {
        LaunchApplication.launch();
    }

    @After
    public void tearDown() {
        GlobalVariable.driver.quit();
    }

    @Test
    public void validLogin() {
        LoginPage loginPage = new LoginPage(GlobalVariable.driver);
        loginPage.fillForm("15261-228947", "15261-228947Test");
        loginPage.login();
        LandingPage landing = new LandingPage(GlobalVariable.driver);
        Assert.assertTrue("Login failed for correct credentials", landing.isTeamDropdownVisible());
    }

    @Test
    public void invalidLogin() {
        LoginPage loginPage = new LoginPage(GlobalVariable.driver);
        loginPage.fillForm("15261228947", "1547Test");
        loginPage.login();
        LandingPage landing = new LandingPage(GlobalVariable.driver);
        Assert.assertFalse("Login success for incorrect credentials", landing.isTeamDropdownVisible());
    }

}