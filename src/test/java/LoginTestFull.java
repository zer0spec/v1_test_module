import org.junit.Test;
import pageObject.LoginPage;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertTrue;

public class LoginTestFull extends LoginPage{

    @Test
    public void validLogin() throws InterruptedException {
        navigateTo(loginPageUrl);
        login(testUser, testValidPass);

        assertTrue("Login Succeeded!", validateCurrentUrl(landingPageUrl));
        assertNotNull(locateElement(nameTeamid));
    }

    @Test
    public void invalidLogin() throws InterruptedException {
        navigateTo(loginPageUrl);
        login(testUser, testInvalidPass);

        assertNotNull(locateElement(classError));

        assertFalse("Login Succeeded Despite This Test Being Negative!", validateCurrentUrl(landingPageUrl));
        assertTrue("Text not found!", locateElement(tagBody).getText().contains(invalidLoginErrorMessage));
    }
}