package pageObject;

import org.openqa.selenium.By;
import pageObject.pageUtilities.AppNavigation;

public class LoginPage extends AppNavigation {
    protected final static String loginPageUrl = baseUrl + "login.jsp";
    protected static final String landingPageUrl = baseUrl + "index.jsp";

    protected final static String invalidLoginErrorMessage = "ERROR: Invalid User ID or Password";

    private static final By idUserid = findById("loginuserid");
    private static final By idPass = findById("loginpass");
    private static final By idLoginButton = findById("login_button");

    protected static By tagBody = findByTagName("body");
    protected static By classError = findByClassName("error");
    protected static By nameTeamid = findByName("teamid");

    public LoginPage() {
    }

    public static void login(final String userName, final String password) throws InterruptedException {
        locateElement(idUserid).sendKeys(userName);
        locateElement(idPass).sendKeys(password);
        locateElement(idLoginButton).click();
        Thread.sleep(5000);
    }
}
