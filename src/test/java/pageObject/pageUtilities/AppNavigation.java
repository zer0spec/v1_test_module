package pageObject.pageUtilities;

import utilities.globalUtilities.ElementUtilities;

public class AppNavigation extends ElementUtilities {
    protected AppNavigation() {
    }

    protected static void navigateTo(final String url) {
        driver.get(url);
    }
}
