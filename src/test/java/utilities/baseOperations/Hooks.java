package utilities.baseOperations;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import utilities.globalUtilities.GlobalVariables;

import java.util.concurrent.TimeUnit;

public class Hooks extends GlobalVariables {
    public static WebDriver driver;
    public static WebElement element;

    private static void launch() {
        System.setProperty("webdriver.chrome.driver", "src/test/java/utilities/base_ops/drivers/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get(baseUrl);
    }

    @BeforeClass
    public static void setUp() { launch(); }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
