package utilities.globalUtilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utilities.baseOperations.Hooks;

import static utilities.baseOperations.Hooks.driver;
import static utilities.baseOperations.Hooks.element;

public class ElementUtilities extends Hooks {
    protected ElementUtilities() {
    }

    protected static By findById(final String element) {
        return By.id(element);
    }

    protected static By findByTagName(final String element) {
        return By.tagName(element);
    }

    protected static By findByClassName(final String element) {
        return By.tagName(element);
    }

    protected static By findByName(final String element) {
        return By.tagName(element);
    }

    protected static Boolean validateCurrentUrl(final String pageUrl) {
        return driver.getCurrentUrl().contains(pageUrl);
    }

    protected static WebElement locateElement(final By elementLocator) {
        try {
            element = driver.findElement(elementLocator);
        } catch (final Exception e) {
            System.out.println("Failed to locate the" + " '" + elementLocator + "' "+ "element");
        }
        return element;
    }
}
