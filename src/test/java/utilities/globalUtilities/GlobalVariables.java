package utilities.globalUtilities;

public class GlobalVariables {
    protected final static String testUser = "auto1";
    protected final static String testValidPass = "eClinicalOS1";
    protected final static String testInvalidPass = "wrongpassword";

    protected final static String baseUrl = "https://val-icd-web.int.etrials.com/";

    protected GlobalVariables() {
    }
}
